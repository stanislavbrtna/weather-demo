import React, { Component } from 'react';
import {Row, Button, Card} from 'react-bootstrap';

class CityWeather extends Component {

  getTimestampString() {
    const settings = {hourCycle: 'h24', hour: '2-digit', minute: '2-digit', second: '2-digit', year: 'numeric', month: 'long', day: 'numeric'};
    return (new Date()).toLocaleDateString('en-US', settings);
  }

  getWeatherData = async () => {
    console.log("looking for weather data");
    const api_call = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.props.name},${this.props.country}&appid=${this.props.apiKey}`);
    const response = await api_call.json();
    
    if (response.cod === 200) {
      this.setState({
        lastUpdate: this.getTimestampString(),
        temperature: response.main.temp - 273.15 ,
        city: response.name,
        country: response.sys.country,
        humidity: response.main.humidity,
        main: response.weather[0].main,
        description: response.weather[0].description,
        error: "",
        errorMsg: ""
      });
    } else {
      this.setState({
        error: "Error occured",
        errorMsg: response.message
      });
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      lastUpdate: this.getTimestampString(),
      expanded: 0,
      temperature: "",
      city: "",
      country: "",
      humidity: "",
      description: "",
      main: "",
      error: "",
      errorMsg: ""
    };
    this.getWeatherData();
    this.removeCity = this.removeCity.bind(this);
    this.expandCity = this.expandCity.bind(this);
  }

  componentDidMount() {
    this.timerID = setInterval( // weather gets updated every hour
      () => this.getWeatherData(),
      1000 * 60 * 60
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  removeCity(e) {
    e.preventDefault();
    this.props.removeCity(this.props.index);
  }

  expandCity(e) {
    e.preventDefault();
    this.setState({
      expanded: 1 - this.state.expanded
    });
  }

  render() {
    if (this.state.error === "") {
    return (
      <Card className="text-left">
        <Card.Body>
          <Card.Title>{this.state.city}</Card.Title>
          <Card.Text>
            <small className="text-muted">{this.state.country}</small>
          </Card.Text>
          
          <Card.Text>{this.state.main}, {Number(this.state.temperature).toFixed(2)} °C</Card.Text>
          
          {this.state.expanded === 1 && <>
            <Card.Text> {this.state.description} </Card.Text>
            <Card.Text> Humidity: {this.state.humidity} % </Card.Text>
            <Card.Text>
              <small className="text-muted"> Last update: {this.state.lastUpdate}</small>
            </Card.Text>
            </>
          }       
          
          {this.state.expanded === 1 &&
            <>
              <Card.Text>
                <Row>
                <div className="col-1"> </div>
                  <Button onClick={this.removeCity} variant="danger" className="col-4">
                    Remove
                  </Button>

                  <div className="col-3"> </div>

                  <Button onClick={this.expandCity} className="col-3">
                    Less
                  </Button>
                </Row>
              </Card.Text>
            </>
          }

          {this.state.expanded === 0 &&
            <>
              <Row>
                <div className="col-8"> </div>
                <Button onClick={this.expandCity} className="col-3">
                  More
                </Button>
              </Row>
            </>
          }
          
        </Card.Body>
      </Card>

    );
    } else {
      return (
        <Card className="text-left">
          <Card.Body>
          <Card.Title>{this.props.name}</Card.Title>
          <Card.Text>
            <small className="text-muted">{this.props.country}</small>
          </Card.Text>
          
          <Card.Text> ERROR occured: {this.state.errorMsg} </Card.Text>
          <Button onClick={this.removeCity} variant="danger">
            Remove
          </Button>
          </Card.Body>
        </Card>
    );

    }
  }
}

export default CityWeather;