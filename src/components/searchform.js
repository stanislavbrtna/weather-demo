import React, { Component } from 'react';
import {Form, Button, Col, Row} from 'react-bootstrap';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    let city = [];
    city.name = e.target.elements.city.value;
    city.country = e.target.elements.country.value;
    if (city.name !== "" && city.country !== "") {
      this.props.addCity(city);
    }
    e.target.elements.city.value = "";
    //e.target.elements.country.value = ""; // might be usefull to keep the previous country
  }
  
  render() {
    return (
    <form onSubmit = {this.handleClick}>
      <Form>
        <Row>
          <Col xs={9} md={8}>
            <Form.Control type="text" name="city" placeholder="City..."/>
            <Form.Control type="text" name="country" placeholder="Country..."/>
          </Col>
          
          <Col xs={1} md={4}>
            <Button type="submit" style={{width: '4rem', height:'4rem', marginTop: '0.4rem'}}><div style={{fontSize: '2.2rem'}}>+</div></Button>
          </Col>
        </Row>
      </Form>
    </form>
    );
  }
}

export default SearchForm;
