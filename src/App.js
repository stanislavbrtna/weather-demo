import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Bootstrap, Grid, Row, Col, Container} from 'react-bootstrap';
import './App.css';
import SearchForm from './components/searchform';
import CityWeather from './components/cityweather';
import {ApiKey} from './apikey';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.addCity = this.addCity.bind(this);
    this.removeCity = this.removeCity.bind(this);
    this.storeChanges = this.storeChanges.bind(this);
    this.loadChanges = this.loadChanges.bind(this);

    this.state = this.loadChanges();
  }

  addCity(city) {
    this.setState({
      cities: [...this.state.cities, city]
    });
    this.storeChanges([...this.state.cities, city]);
  }

  removeCity(key) {
    let tmpCities = this.state.cities;
    tmpCities.splice(key, 1);
    console.log("splicing:" + key);
    console.log(tmpCities);
    this.setState({
      cities: tmpCities
    });
    this.storeChanges(tmpCities);
  }

  storeChanges(list) {
    // JSON.stringify(list) didn't work... something to do with it being an array of objects or something...
    // so I am doing it the stupid way
    let names = {};
    let locations = {};

    names = list.map((city, i) => {
        return city.name;
      }
    );

    locations = list.map((city, i) => {
        return city.country;
      }
    );

    localStorage.setItem('cityNames', JSON.stringify(names));
    localStorage.setItem('cityLocations', JSON.stringify(locations));
  }

  loadChanges() {
    if (localStorage.getItem('cityLocations') && localStorage.getItem('cityNames')) {
      console.log("Loading:");
      let tmpCity = [];

      let tmpNames = JSON.parse(localStorage.getItem('cityNames'));
      let tmpLocations = JSON.parse(localStorage.getItem('cityLocations'));

      if (tmpNames.length !== tmpLocations.length) {
        return {cities: []};
      }

      for(let i = 0; i < tmpNames.length; i++) {
        let city = [];
        city.name = tmpNames[i];
        city.country = tmpLocations[i];
        tmpCity = [...tmpCity, city];
      }

      console.log(localStorage.getItem('cityList'));
      return {cities: tmpCity};
    } else {
      return {cities: []};
    }
  }

  render() {

    const cityTables = this.state.cities.map((city, i) =>
      <Col  xs={12} md={{ span: 4 }} key={city.name+city.country}  className="mt-2 mb-2" > 
        <CityWeather index={i} name={city.name} country={city.country} removeCity={this.removeCity.bind(this)} apiKey={ApiKey} />
      </Col>
    );

    return (
      <>
      <Container>
      <Row>
        <Col>
          <h1 className="mt-3">Weather demo</h1>
        </Col>
      </Row>
      </Container>

      <div style={{backgroundColor: 'rgba(0, 66, 255, 0.12)'}}>
        <Container>
        <Row>
            {cityTables}
        </Row>
        </Container>
      </div>

      <Container>
        <Row className="mt-3">
          <div className="col-md-8"></div>
          <div className="col-md-4">
            <SearchForm addCity={this.addCity.bind(this)} />
          </div>
        </Row>
      </Container>
      </>
    );
  }
}

export default App;
