# Weather demo
Simple weather app written in React. It uses Bootstrap 4 and react-bootstrap for css things.

## Set up
Just clone this repo, rename *apikey.js.example* to *apikey.js* and replace the placeholder OpenWeatherMap key with your own key.
Then run:

    npm install
    npm run start

The app will get all its things and will start in your browser.

## Build
For deployment run:
(Don't forget to change app home directory to your own in *package.json*)

    npm run build
 
 Enjoy!